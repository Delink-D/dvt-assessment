import { NgModule } from '@angular/core';
import { CompactNumber } from './compact-number.pipe';


@NgModule({
  declarations: [CompactNumber],
  exports: [CompactNumber]
})
export class PipesModule { }
