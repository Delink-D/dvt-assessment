import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'compactNumber', pure: false })
export class CompactNumber implements PipeTransform {
  transform(number: number): any {
    //@ts-ignore
    const formatter = new Intl.NumberFormat('en-GB', { notation: 'compact', compactDisplay: 'short' });
    return formatter.format(number);
  }
}