import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';

import { PipesModule } from './pipes/pipes.module';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { HomeModule } from './pages/home/home.module';
import { ArtistDetailsModule } from './pages/details/artist/artist-details.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    PipesModule,
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HomeModule,
    ArtistDetailsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
