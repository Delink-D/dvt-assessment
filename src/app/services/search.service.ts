import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Artist } from '../models/artist';

@Injectable({
  providedIn: 'root'
})
export class SearchService {
  API = environment.API_PATH; // API_PATH

  constructor(private http: HttpClient) { }

  /** Function search artist from API end point */
  searchArtist(searchTerm: string): Promise<any> {
    return new Promise((resolve, reject) => {

      let list: any = [];
      this.http.get(`/search?q=${searchTerm}`).subscribe({
        next: (res: any) => {
          list = res.data;
        },
        error: (error: any) => {
          reject(error);
        },
        complete: () => {
          resolve(list);
        }
      });
    });
  }

  /** function get a single artist details */
  getArtist(id: number): Promise<Artist> {
    return new Promise((resolve, reject) => {
      let artist: any = [];
      this.http.get(`/artist/${id}`).subscribe({
        next: (res: any) => {
          artist = res;
        },
        error: (error: any) => {
          reject(error);
        },
        complete: () => {
          resolve(artist);
        }
      });
    });
  }

  /** function get artist track list */
  getTrackList(id: number): Promise<any> {
    return new Promise((resolve, reject) => {
      let topTen: any;
      this.http.get(`/artist/${id}/top?limit=10`).subscribe({
        next: (res: any) => {
          topTen = res.data;
        },
        error: (error: any) => {
          reject(error);
        },
        complete: () => {
          resolve(topTen);
        }
      })
    })
  }

  /** function get artist albums list */
  getAlbums(id: number): Promise<any> {
    return new Promise((resolve, reject) => {
      let albums: any;
      this.http.get(`/artist/${id}/albums`).subscribe({
        next: (res: any) => {
          albums = res.data;
        },
        error: (error: any) => {
          reject(error);
        },
        complete: () => {
          resolve(albums);
        }
      })
    })
  }
}
