export interface Artist {
  id: number;
  nb_fan: number;
  artist: Artist;
  album: Album;
}

export interface Artist {
  id: number;
  link: string;
  name: string;
  picture: string;
  picture_big: string;
  picture_medium: string;
  picture_small: string;
  picture_xl: string;
  trackList: string;
}

export interface Album {
  id: number
  cover: string;
  cover_big: string;
  cover_medium: string;
  cover_small: string;
  cover_xl: string;
  title: string;
  trackList: string;
}