import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';

import { AppMaterialModule } from '../../../material/app-material.module';

import { ArtistDetailsComponent } from './artist-details.component';
import { PipesModule } from '../../../pipes/pipes.module';

@NgModule({
  declarations: [
    ArtistDetailsComponent
  ],
  imports: [
    PipesModule,
    FormsModule,
    CommonModule,
    ReactiveFormsModule,
    AppMaterialModule,
    FlexLayoutModule
  ],
  providers: [

  ]
})
export class ArtistDetailsModule { }
