import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Artist } from 'src/app/models/artist';
import { SearchService } from '../../../services/search.service';

@Component({
  selector: 'app-artist-details',
  templateUrl: './artist-details.component.html',
  styleUrls: ['./artist-details.component.css']
})
export class ArtistDetailsComponent implements OnInit {
  artistId!: number;
  artist!: Artist;
  trackList!: any;
  artistAlbums!: any;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private searchService: SearchService
  ) {
    const urlValue = this.route.snapshot.paramMap.get('id');
    this.artistId = +(urlValue ? urlValue : '');
  }

  ngOnInit(): void {
    this.getArtist();
  }

  /** function get artist details */
  getArtist() {

    this.searchService.getArtist(this.artistId).then(artist => {
      this.artist = artist;
      this.getTrackList(this.artist.id);   // get track list
      this.getAlbumList(this.artist.id);   // get album list
    }).catch(error => {
      console.log('Error:', error)
    })
  }

  /** function get top track list */
  getTrackList(id: number) {
    this.searchService.getTrackList(id).then(list => {
      this.trackList = list;
    }).catch(error => console.log(error))
  }

  /** function get albums list */
  getAlbumList(id: number) {
    this.searchService.getAlbums(id).then(list => {
      this.artistAlbums = list;
    }).catch(error => console.log(error))
  }

  /** function take me back home */
  goBackHome() {
    this.router.navigate(['/home']);
  }
}
