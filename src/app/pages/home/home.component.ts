import { Component } from '@angular/core';
import { Artist } from '../../models/artist';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {
  listOfSearch!: Artist[];
  searching: boolean = false;

  constructor() { }

  /** set the artist for the component */
  searchedArtists(e: any) {
    this.listOfSearch = e;
  }
}
