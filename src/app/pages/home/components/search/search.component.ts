import { Component, EventEmitter, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';

import { Artist } from '../../../../models/artist';
import { SearchService } from '../../../../services/search.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent {
  searchForm!: FormGroup;
  searchTerm!: string;

  artists!: Artist[];

  @Output() listOfSearch: EventEmitter<any> = new EventEmitter<any>();
  @Output() searching: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(
    private fb: FormBuilder,
    private snackBar: MatSnackBar,
    private searchService: SearchService
  ) {
    this.searchForm = this.fb.group({
      searchTerm: ['', [Validators.required]]
    });
  }

  /** Function search for an artist */
  searchArtist() {
    this.searching.emit(true);

    this.searchService.searchArtist(this.searchTerm).then((artist: Artist[]) => {
      this.artists = artist;
      console.log(this.artists);
      this.listOfSearch.emit(this.artists);
      this.searching.emit(false);
    }).catch(error => {
      this.snackBar.open('Error searching an artist');
    })
  }
}
