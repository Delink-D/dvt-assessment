import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';

import { AppMaterialModule } from '../../material/app-material.module';

import { HomeComponent } from './home.component';
import { NavigationComponent } from './components/navigation/navigation.component';
import { SearchComponent } from './components/search/search.component';
import { SearchService } from '../../services/search.service';

@NgModule({
  declarations: [
    HomeComponent,
    NavigationComponent,
    SearchComponent
  ],
  imports: [
    FormsModule,
    CommonModule,
    RouterModule,
    ReactiveFormsModule,
    AppMaterialModule,
    FlexLayoutModule
  ],
  providers: [
    SearchService
  ],
  exports: [NavigationComponent]
})
export class HomeModule { }
